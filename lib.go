package teamsnote

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type MessageCard struct {
	Type       string    `json:"@type"`
	Context    string    `json:"@context"`
	ThemeColor string    `json:"themeColor"`
	Summary    string    `json:"summary"`
	Sections   []Section `json:"sections"`
	Actions    []Action  `json:"potentialAction"`
}

func NewMessageCard(
	summary string,
	sections []Section,
	actions []Action,
) MessageCard {
	return MessageCard{
		Type:       "MessageCard",
		Context:    "http://schema.org/extensions",
		ThemeColor: "000000",
		Summary:    summary,
		Sections:   sections,
		Actions:    actions,
	}
}

type Section struct {
	ActivityTitle    string `json:"activityTitle"`
	ActivitySubtitle string `json:"activitySubtitle"`
	ActivityImageURI string `json:"activityImage"`
	Facts            []Fact `json:"facts"`
}

type Fact struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

// Action is a sum type, but only those of OpenUri are listed.
type Action struct {
	Type    string   `json:"@type"`
	Name    string   `json:"name"`
	Targets []Target `json:"targets"`
}

func NewOpenURIAction(name string, targets []Target) Action {
	return Action{
		Type:    "OpenUri",
		Name:    name,
		Targets: targets,
	}
}

type Target struct {
	OS  string `json:"os"`
	URI string `json:"uri"`
}

func NewTarget(uri string) Target {
	return Target{
		OS:  "default",
		URI: uri,
	}
}

type SendNotificationFunc func(ctx context.Context, card MessageCard) error

func SendTeamsNotification(
	webHookURL string,
) SendNotificationFunc {
	return func(ctx context.Context, card MessageCard) error {
		cardBody, err := json.Marshal(card)
		if err != nil {
			return err
		}

		request, err := http.NewRequest(
			http.MethodPost,
			webHookURL,
			bytes.NewReader(cardBody),
		)
		request.Header.Add("Content-Type", "application/json")

		response, err := http.DefaultClient.Do(request.WithContext(ctx))
		if err != nil {
			return err
		}

		if response.StatusCode < 200 || response.StatusCode >= 300 {
			responseBody, err := io.ReadAll(response.Body)
			if err != nil {
				return fmt.Errorf(
					"teams notification request failed with code %d",
					response.StatusCode,
				)
			}
			return fmt.Errorf(
				"teams notification request failed with code %d: %s",
				response.StatusCode,
				string(responseBody),
			)
		}

		return nil
	}
}
